liblib-abs-perl (0.95-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + liblib-abs-perl: Add Multi-Arch: foreign.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 20:02:20 +0000

liblib-abs-perl (0.95-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Damyan Ivanov ]
  * change Priority from 'extra' to 'optional'

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.95.
  * Update years of upstream copyright.
  * Add debian/upstream/metadata.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 4.5.0.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Update build dependencies.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Fri, 24 Jan 2020 16:10:43 +0100

liblib-abs-perl (0.93-1) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * debian/watch: use dist-based URL.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * New upstream release.
  * Switch from cdbs to dh(1).
  * Bump debhelper compatibility level to 8.
  * Improve short and long description.
  * Use metacpan URLs.
  * Update years of upstream and third-party copyright.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Wed, 01 Jan 2014 23:24:35 +0100

liblib-abs-perl (0.92-4) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Correct Vcs-Git and Vcs-Browser control fields.
    Thanks to Jari Aalto <jari.aalto@cante.net> for the report.
    (Closes: #670286)

  [ Dmitry E. Oboukhov ]
  * Upload the package.

 -- Dmitry E. Oboukhov <unera@debian.org>  Tue, 03 Jul 2012 01:05:38 +0400

liblib-abs-perl (0.92-3) unstable; urgency=low

  * Add debian/watch.

 -- Dmitry E. Oboukhov <unera@debian.org>  Tue, 15 Feb 2011 12:07:27 +0300

liblib-abs-perl (0.92-2) unstable; urgency=low

  * Add debian-perl to Maintainer record.
  * Use git.debian.org.

 -- Dmitry E. Oboukhov <unera@debian.org>  Tue, 15 Feb 2011 11:32:56 +0300

liblib-abs-perl (0.92-1) unstable; urgency=low

  * Initial release. (Closes: #612722)

 -- Dmitry E. Oboukhov <unera@debian.org>  Mon, 07 Feb 2011 17:57:12 +0300
